library(jsonlite)


get_cats_versions <- function() {
    return(jsonlite::fromJSON(
        "https://gitlab.phaidra.org/bdc/catsr/-/raw/main/data/cats.json"
    ))
}


is_directory_empty <- function(path) {
    if (!dir.exists(path)) {
        stop(sprintf("Path '%s' is not a directory", path))
    }

    contents <- list.files(path, all.files=TRUE, include.dirs=TRUE, no..=TRUE)

    return (length(contents) == 0)
}


#' Downloads the newest version of the CATS quick start data
#'
#' @param install.dir Path were the data should be downloaded to and installed in.
#' @param redownload Logical, default FALSE. If set to TRUE, data will be re-downloaded,
#' even if it already has been downloaded
#' @param reextract Logical, default FALSE. If set to TRUE, the downloaded data will be re-extracted, even if the target already exists
#' @return Path to the data (a subdirectory of `install.dir`)
#' @export
#'
download_cats_quickstart_data <- function(install.dir, redownload = F, reextract = F) {
    data <- get_cats_versions()

    if (!dir.exists(install.dir)) {
        dir.create(install.dir)
    }
    newest_version <- data$newest$quickstart_data
    newest_url <-
        subset(data$quickstart_data, version == newest_version)$url
    destination_file <- file.path(install.dir, sprintf("cats-quickstart-data-%s.zip", newest_version))
    if (!file.exists(destination_file) || redownload == TRUE) {
        print(sprintf("Downloading %s to %s", newest_url, destination_file))
        timeout <- R.utils::getOption("timeout")
        R.utils::setOption("timeout", 3600)
        result <-
            utils::download.file(newest_url, destfile = destination_file, mode="wb")
        R.utils::setOption("timeout", timeout)

        if (result != 0) {
            stop("Error downloading CATS quick start data")
        }
    } else {
        print(sprintf("Reusing existing file %s", destination_file))
    }

    result_path <- file.path(install.dir, "example")
    if (file.exists(result_path) && reextract == FALSE) {
        print(sprintf("Reusing existing directory %s", result_path))
    } else {
        print(sprintf(
            "Extracting %s to directory %s",
            destination_file,
            install.dir
        ))
        utils::unzip(destination_file, exdir = install.dir)
    }


    return(result_path)
}

#' Download and installs the newest version of CATS for Windows
#'
#' This function is only exported for testing purposes, please
#' use [download_and_install_cats()]
#'
#' @param install.dir The directory to which CATS should be downloaded
#' @param redownload Logical, default FALSE. Whether CATS should be re-downloaded,
#' if the archive file already exists.
#' @param reextract Logical, default FALSE. Whether the data should be reextracted,
#' if it already exists
#' @return Path to the installation directory (a subdirectory of `install.dir`)
#' @export
download_cats_for_windows <- function(install.dir, redownload = F, reextract = F ) {



    if (!dir.exists(install.dir)) {
        dir.create(install.dir)
    }
    install.dir <- normalizePath(install.dir)

    if (!is_directory_empty(install.dir)) {
        stop(sprintf("Directory %s is not empty. Please specify an empty directory.", install.dir))
    }

    data <- get_cats_versions()

    newest_version <- data$newest$windows_binaries
    newest_url <-
        subset(data$windows_binaries, version == newest_version)$url

    destination_file <- file.path(install.dir, sprintf("cats-%s-windows.zip", newest_version))
    if (!file.exists(destination_file) || redownload == TRUE) {
        print(sprintf("Downloading %s to %s", newest_url, destination_file))
        timeout <- R.utils::getOption("timeout")
        result <-
            utils::download.file(newest_url, destfile = destination_file, mode="wb")
        R.utils::setOption("timeout", timeout)
        if (result != 0) {
            stop("Error downloading CATS")
        }
    } else {
        print(sprintf("Reusing existing file %s", destination_file))
    }



    result_path <- file.path(install.dir, sprintf("cats-windows-%s", newest_version))

    if (file.exists(result_path) && reextract == FALSE) {
        print(sprintf("Reusing existing directory %s", result_path))
    } else {
        print(sprintf(
            "Extracting %s to directory %s",
            destination_file,
            install.dir
        ))
        utils::unzip(destination_file, exdir = install.dir)
    }

    return(result_path)
}


#' Download and install CATS
#'
#' Downloads and installs the newest version of CATS.
#' Currently only the Windows operating system is supported. The installation of CATS
#' is restricted to the installation directory, not changes outside that location
#' are made. To uninstall, delete the directory.
#' For other operations system CATS is should be downloaded and built from source.
#'
#' @param install.dir The directory to which CATS should be downloaded
#' @param redownload Logical, default FALSE. Whether CATS should be re-downloaded,
#' if the archive file already exists.
#' @param reextract Logical, default FALSE. Whether the data should be reextracted,
#' if it already exists
#'
#' @return Path to the installation directory (a subdirectory of `install.dir`)
#' @export
download_and_install_cats <- function(install.dir, redownload = F, reextract = F) {
    if (.Platform$OS.type == "windows") {
        return(download_cats_for_windows(install.dir, redownload = redownload, reextract=reextract))
    } else {
        stop(
            sprintf(
                "Can't download precompiled binary for this platform (%s). Please download and install CATS from source, available from https://cats.univie.ac.at",
                Sys.info()["sysname"]
            )
        )
    }
}
