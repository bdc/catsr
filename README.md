# catsr

catsr is a thin wrapper around the [CATS](https://cats.univie.ac.at) modelling framework,  which was designed to run stand-alone. 
This package was written to facilitate running CATS simulations.

